# Solusoft

## Ambiente Utilizando no Desenolvimento
```bash
Container Docker Linux utilizando:
PHP 7.2
Apache/2.4.25 (Debian)
MariaDB 10.3
```

## Instação

Após fazer download do projeto, utilizar composer para instalar dependências

```bash
$ composer install
```

Adicionar um registro ao vhost, modificar Directory e DocumentRoot para diretorio correspondente ao Apache

```bash
<VirtualHost *:80>
  ServerName prova.localhost
  ServerAlias prova.localhost
  DocumentRoot "/app/prova-solusoft"
  UseCanonicalName Off
</VirtualHost>
```

Editar arquivo hosts do Sistema

```bash
Abra a pasta %WinDir%\System32\Drivers\Etc (Windows) ou /etc/hosts (Linux)
Abrir arquivo hosts
Adicionar a seguinte linha ao arquivo e salvar
127.0.0.1 prova.local
```

Editar arquivos configurações CodeIgniter config.php e database.php conforme necessidade.
Por ser tratar de uma prova simples, optei por não colocar essas informações em variáveis de ambiente.

Arquivo de Dump do banco se encontra na raiz com o nome schema.sql

## Observações
```bash
Para simplificar o sistema, optei por não usar modals ou mensagens de confirmação.
Foi utilizado o .clone do JQuery para o form de pedidos, o que não é o ideal, em uma aplicacação real, utilizaria função pré-definida com todo o html a ser replicado mais um componente de auto-complete como o select2 .
```