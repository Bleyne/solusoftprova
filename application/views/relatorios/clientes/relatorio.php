<html>
<head>
    <style>
        table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        td, th {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
        }

        tr:nth-child(even) {
            background-color: #dddddd;
        }
    </style>
</head>
<body>

<h1>Relatório Clientes</h1>
Relatório feito utilizando a lib MPDF
<div>
    <h4>Pedidos do cliente <?php echo $clienteNome; ?></h4>
    <table>
        <tr>
            <th>Nº Pedido</th>
            <th>Data</th>
            <th>Total</th>
        </tr>
        <?php foreach ($data as $row) { ?>
        <tr>
            <td><?php echo $row['numero_pedido']; ?></td>
            <td><?php echo convertDatePtBr($row['pedido_data']); ?></td>
            <td>R$ <?php echo $row['total_pedido']; ?></td>
        </tr>
        <?php } ?>
    </table>
</div>
</body>
</html>