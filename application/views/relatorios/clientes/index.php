<div class="container">
    <section class="jumbotron text-center">
        <h1 class="jumbotron-heading">Relatório Cliente</h1>
        <p class="lead text-muted">Filtro de Pedidos do Cliente</p>
    </section>

    <form id="relatorio-form" action="<?php echo base_url();?>relatorio/clientes/view" method="post">
        <div class="row">
            <div class="col">
                <div class="form-group">
                    <label for="cliente">Cliente</label>
                    <select id="cliente" class="form-control" name="cliente_id" required>
                        <option value="" selected>Selecione...</option>
                        <?php foreach ($clientes as $cliente) { ?>
                            <option value="<?php echo $cliente['id']; ?>"><?php echo $cliente['nome']; ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>
        </div>
        <button type="submit" class="btn btn-primary">Enviar</button>
    </form>
</div>

<script>
    $(document).ready(function () {
        new window.SetMascara();
    });
</script>