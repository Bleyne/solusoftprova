<div class="container">
    <section class="jumbotron text-center">
        <h1 class="jumbotron-heading">Relatório Pedidos</h1>
        <p class="lead text-muted">Filtro de Pedidos por data</p>
    </section>

    <form id="relatorio-form" action="<?php echo base_url();?>relatorio/pedidos/view" method="post">
        <div class="row">
            <div class="col">
                <div class="form-group">
                    <label for="data_inicial">Data Inicial</label>
                    <input type="text" class="form-control date" id="data_inicial" name="data_inicial" placeholder="Data Inicial" required>
                </div>
            </div>
            <div class="col">
                <div class="form-group">
                    <label for="data_final">Data Final</label>
                    <input type="text" class="form-control date" id="data_final" name="data_final" placeholder="Data Final" required>
                </div>
            </div>
        </div>
        <button type="submit" class="btn btn-primary">Enviar</button>
    </form>
</div>

<script>
    $(document).ready(function () {
        new window.SetMascara();
    });
</script>