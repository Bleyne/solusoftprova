<html>
<head>
    <style>
        table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        td, th {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
        }

        tr:nth-child(even) {
            background-color: #dddddd;
        }
    </style>
</head>
<body>

<h1>Relatório Pedidos</h1>
Relatório feito utilizando a lib MPDF
<div>
    <h4>Pedidos realizados entre <?php echo "$dataInicial - $dataFinal"; ?></h4>
    <table>
        <tr>
            <th>Nº Pedido</th>
            <th>Data</th>
        </tr>
        <?php foreach ($result as $row) { ?>
        <tr>
            <td><?php echo $row->id; ?></td>
            <td><?php echo convertDatePtBr($row->data); ?></td>
        </tr>
        <?php } ?>
    </table>
</div>
</body>
</html>