<div class="container">
    <section class="jumbotron text-center">
        <h1 class="jumbotron-heading">Lista Clientes</h1>
        <p class="lead text-muted">Listagem de clientes utilizando lib DataTables</p>
    </section>

    <div id="alert" class="alert alert-success text-center" role="alert" style="display: none;">
        Cliente Removido
    </div>

    <table id="cliente-table" class="table table-striped table-bordered" style="width:100%">
        <thead>
        <tr>
            <th>ID</th>
            <th>Nome</th>
            <th>CPF</th>
            <th>Sexo</th>
            <th>Email</th>
            <th>Ações</th>
        </tr>
        </thead>
    </table>
</div>
<script>

    function remover(link) {
        $.ajax({url: link, type: 'GET'})
            .done(function (response) {
                if (response.data === 'OK') {
                    $('#alert').show();
                    window.table.draw();
                }
            });
    }

    $(document).ready(function () {
        window.table = $('#cliente-table').DataTable({
            "processing": true,
            "serverSide": true,
            "searching": false,
            "language": {
                "url": "/js/dataTables-pt-br.json"
            },
            "ajax": "/clientes/data"
        });
    });
</script>