<div class="container">
    <section class="jumbotron text-center">
        <h1 class="jumbotron-heading">Editar Clientes</h1>
        <p class="lead text-muted">Formulário destinado edição de clientes.</p>
    </section>


    <div id="alert" class="alert alert-success text-center" role="alert" style="display: none;">
        Cliente Atualizado
    </div>


    <form id="clientes-form">
        <input type="hidden" name="id" value="<?php echo $id; ?>">
        <div class="row">
            <div class="col">
                <div class="form-group">
                    <label for="nome">Nome</label>
                    <input type="text" class="form-control" id="nome" name="nome" placeholder="Nome" value="<?php echo $nome; ?>" required>
                </div>
            </div>
            <div class="col">
                <div class="form-group">
                    <label for="email">Email</label>
                    <input type="email" class="form-control" id="email" name="email" placeholder="Email" value="<?php echo $email; ?>" required>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div class="form-group">
                    <label for="cpf">CPF</label>
                    <input type="text" class="form-control" id="cpf" name="cpf" placeholder="CPF" value="<?php echo $cpf; ?>" required>
                </div>
            </div>
            <div class="col">
                <div class="form-group">
                    <label for="sexo">Sexo</label>
                    <select id="sexo" class="form-control" name="sexo" required>
                        <option value="">Selecione...</option>
                        <option value="0" <?php if ($sexo == 0) echo "selected"; ?>>Masculino</option>
                        <option value="1"  <?php if ($sexo == 1) echo "selected"; ?>>Feminino</option>
                    </select>
                </div>
            </div>
        </div>
        <button type="submit" class="btn btn-primary">Enviar</button>
    </form>
</div>

<script>
    $(document).ready(function () {
        new window.SetMascara();
    });

    $("#clientes-form").submit(function (event) {
        event.preventDefault();
        let form_data = $(this).serialize();

        $.ajax({
            url: '/api/clientes/editar',
            type: 'POST',
            data: form_data
        }).done(function (response) {
            if (response.data === 'OK') {
                $('#alert').show();
            }
        });
    });
</script>