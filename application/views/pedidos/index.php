<div class="container">
    <section class="jumbotron text-center">
        <h1 class="jumbotron-heading">Lista Pedidos</h1>
        <p class="lead text-muted">Listagem de pedidos utilizando lib DataTables</p>
    </section>

    <div id="alert" class="alert alert-success text-center" role="alert" style="display: none;">
        Pedido Removido
    </div>

    <div id="alert-email" class="alert alert-success text-center" role="alert" style="display: none;">
        Pedido Enviado por E-mail
    </div>

    <table id="pedido-model" class="table table-striped table-bordered" style="width:100%">
        <thead>
        <tr>
            <th>ID</th>
            <th>Cliente</th>
            <th>Data</th>
            <th>Ações</th>
        </tr>
        </thead>
    </table>
</div>
<script>

    function remover(link) {
        $.ajax({url: link, type: 'GET'})
            .done(function (response) {
                if (response.data === 'OK') {
                    $('#alert').show();
                    window.table.draw();
                }
            });
    }

    function enviarEmail(link) {
        $('#alert-email').hide();
        $.ajax({url: link, type: 'GET'})
            .done(function (response) {
                if (response.data === 'OK') {
                    $('#alert-email').show();
                }
                else {
                    alert('Houve um erro no envio de email: Cheque as configurações SMTP e verifique o console javascript para mais informações.');
                    console.error(response);
                }
            });
    }

    $(document).ready(function () {
        window.table = $('#pedido-model').DataTable({
            "processing": true,
            "serverSide": true,
            "searching": false,
            "language": {
                "url": "/js/dataTables-pt-br.json"
            },
            "ajax": "/pedidos/data"
        });
    });
</script>