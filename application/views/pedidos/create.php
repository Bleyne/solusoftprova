<div class="container">
    <section class="jumbotron text-center">
        <h1 class="jumbotron-heading">Criar Pedidos</h1>
        <p class="lead text-muted">Formulário destinado criação de pedidos.</p>
    </section>


    <div id="alert" class="alert alert-success text-center" role="alert" style="display: none;">
        Pedido Criado
    </div>


    <form id="pedidos-form">
        <div class="row">
            <div class="col">
                <div class="form-group">
                    <label for="cliente">Cliente</label>
                    <select id="cliente" class="form-control" name="cliente_id" required>
                        <option value="" selected>Selecione...</option>
                        <?php foreach ($clientes as $cliente) { ?>
                            <option value="<?php echo $cliente['id']; ?>"><?php echo $cliente['nome']; ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>
            <div class="col">
                <div class="form-group">
                    <label for="forma_pagamento">Forma Pagamento</label>
                    <select id="forma_pagamento" class="form-control" name="forma_pagamento" required>
                        <option value="" selected>Selecione...</option>
                        <option value="dinheiro">Dinheiro</option>
                        <option value="cartao">Cartão</option>
                        <option value="cheque">Cheque</option>
                    </select>
                </div>
            </div>
        </div>
        <fieldset>
            <legend>Pedido de Produtos</legend>
            <div class="row produtos" id="produto-area">
                <div class="col">
                    <div class="form-group">
                        <label for="produto">Produto</label>
                        <select class="form-control" name="produto[]" required>
                            <option value="" selected>Selecione...</option>
                            <?php foreach ($produtos as $produto) { ?>
                                <option value="<?php echo $produto['id']; ?>"><?php echo $produto['nome']; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="col">
                    <div class="form-group">
                        <label for="quantidade">Quantidade</label>
                        <input type="number" class="form-control" name="quantidade[]" placeholder="Quantidade" required>
                    </div>
                </div>
            </div>

            <div id="clone-area"></div>

            <button type="button" class="btn btn-info" onclick="addProduto();">+ Adicionar Produto</button>
            <button type="button" class="btn btn-danger" onclick="removerProduto();">- Remover Produto</button>

        </fieldset>
        <div class="row">
            <div class="col">
                <div class="form-group">
                    <label for="observacao">Observação</label>
                    <textarea class="form-control" id="observacao" name="observacao" rows="3"></textarea>
                </div>
            </div>
        </div>
        <button type="submit" class="btn btn-primary">Enviar</button>
    </form>
</div>

<script>
    function addProduto() {
        $("#produto-area").clone().appendTo("#clone-area");
    }

    function removerProduto() {
        if ($(".produtos").length == 1) {
            alert('Produto não pode ser removido. Necessário pelo menos um produto no pedido.')
        } else {
            $("#produto-area").remove();
        }
    }

    $("#pedidos-form").submit(function (event) {
        event.preventDefault();
        let form_data = $(this).serialize();

        $.ajax({
            url: '/api/pedidos/criar',
            type: 'POST',
            data: form_data
        }).done(function (response) {
            if (response.data === 'OK') {
                $('#alert').show();
                $('#pedidos-form')[0].reset();
                $("#produto-area").empty()
            }
        });
    });
</script>