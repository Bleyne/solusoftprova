<div class="container">
    <section class="jumbotron text-center">
        <h1 class="jumbotron-heading">Editar Produtos</h1>
        <p class="lead text-muted">Formulário destinado edição de produtos.</p>
    </section>


    <div id="alert" class="alert alert-success text-center" role="alert" style="display: none;">
        Produto Atualizado
    </div>


    <form id="produtos-form">
        <input type="hidden" name="id" value="<?php echo $id; ?>">
        <div class="row">
            <div class="col">
                <div class="form-group">
                    <label for="nome">Nome</label>
                    <input type="text" class="form-control" id="nome" name="nome" placeholder="Nome" value="<?php echo $nome; ?>" required>
                </div>
            </div>
            <div class="col">
                <div class="form-group">
                    <label for="cor">Cor</label>
                    <input type="text" class="form-control" id="cor" name="cor" placeholder="Cor" value="<?php echo $cor; ?>" required>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div class="form-group">
                    <label for="tamanho">Tamanho</label>
                    <input type="text" class="form-control" id="tamanho" name="tamanho" placeholder="Tamanho" value="<?php echo $tamanho; ?>" required>
                </div>
            </div>
            <div class="col">
                <div class="form-group">
                    <label for="valor">Valor</label>
                    <input type="text" class="form-control money" id="valor" name="valor" placeholder="Valor" value="<?php echo $valor; ?>" required>
                </div>
            </div>
        </div>
        <button type="submit" class="btn btn-primary">Enviar</button>
    </form>
</div>

<script>
    $(document).ready(function () {
        new window.SetMascara();
    });

    $("#produtos-form").submit(function (event) {
        event.preventDefault();
        let form_data = $(this).serialize();

        $.ajax({
            url: '/api/produtos/editar',
            type: 'POST',
            data: form_data
        }).done(function (response) {
            if (response.data === 'OK') {
                $('#alert').show();
            }
        });
    });
</script>