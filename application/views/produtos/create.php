<div class="container">
    <section class="jumbotron text-center">
        <h1 class="jumbotron-heading">Criar Produtos</h1>
        <p class="lead text-muted">Formulário destinado criação de produtos.</p>
    </section>


    <div id="alert" class="alert alert-success text-center" role="alert" style="display: none;">
        Produto Criado
    </div>


    <form id="produtos-form">
        <div class="row">
            <div class="col">
                <div class="form-group">
                    <label for="nome">Nome</label>
                    <input type="text" class="form-control" id="nome" name="nome" placeholder="Nome" required>
                </div>
            </div>
            <div class="col">
                <div class="form-group">
                    <label for="cor">Cor</label>
                    <input type="text" class="form-control" id="cor" name="cor" placeholder="Cor" required>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div class="form-group">
                    <label for="tamanho">Tamanho</label>
                    <input type="text" class="form-control" id="tamanho" name="tamanho" placeholder="Tamanho" required>
                </div>
            </div>
            <div class="col">
                <div class="form-group">
                    <label for="valor">Valor</label>
                    <input type="text" class="form-control money" id="valor" name="valor" placeholder="Valor" required>
                </div>
            </div>
        </div>
        <button type="submit" class="btn btn-primary">Enviar</button>
    </form>
</div>

<script>
    $(document).ready(function () {
        new window.SetMascara();
    });

    $("#produtos-form").submit(function (event) {
        event.preventDefault();
        let form_data = $(this).serialize();

        $.ajax({
            url: '/api/produtos/criar',
            type: 'POST',
            data: form_data
        }).done(function (response) {
            if (response.data === 'OK') {
                $('#alert').show();
                $('#produtos-form')[0].reset();
            }
        });
    });
</script>