<html>
<head>
    <style>
        table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        td, th {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
        }

        tr:nth-child(even) {
            background-color: #dddddd;
        }
    </style>
</head>
<body>

<h1>Pedido Nº <?php echo $numeroPedido; ?></h1>

<div>
    <h4>Pedido realizado dia <?php echo convertDatePtBr($pedidoData); ?></h4>
    <table>
        <tr>
            <th>Produto</th>
            <th>Quantidade</th>
            <th>Total</th>
        </tr>
        <?php foreach ($pedidoDetalhes as $row) { ?>
            <tr>
                <td><?php echo $row['nome_produto']; ?></td>
                <td><?php echo $row['qtde']; ?></td>
                <td> R$ <?php echo convertDateMySqlconvertDinheiro($row['qtde'] * $row['valor']); ?></td>
            </tr>
        <?php } ?>
    </table>
</div>
</body>
</html>