<?php

defined('BASEPATH') OR exit('No direct script access allowed');

if (!function_exists('convertDatePtBr')) {

    function convertDatePtBr($date) {
        return implode("/",array_reverse(explode("-",$date)));
    }
    
}

if (!function_exists('convertDateMySql')) {

    function convertDateMySql($date) {
        return implode("-",array_reverse(explode("/",$date)));
    }

}

if (!function_exists('convertNumeroDecimal')) {

    function convertNumeroDecimal($value){
        $value = str_replace(".", "", $value);
        return str_replace(",", ".", $value);
    }

}

if (!function_exists('convertDinheiro')) {

    function convertDateMySqlconvertDinheiro($value)
    {
        return number_format($value, 2, ',', '.');
    }

}