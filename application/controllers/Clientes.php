<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Clientes extends CI_Controller {

    const SEXO_MASCULINO = 0;
    const SEXO_FEMININO = 1;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('clientes_model');
        $this->load->helper('url_helper');
    }

	public function index()
	{
        $this->load->view('templates/header');
        $this->load->view('clientes/index');
        $this->load->view('templates/footer');
	}

    public function data()
    {
        $draw = intval($this->input->get("draw"));
        $data = $this->clientes_model->get();

        $tableData = array();
        foreach ($data->result() as $row) {
            $actions = '<div class="row">';
            $editLink = base_url(). "clientes/editar/{$row->id}";
            $editAnchor = "<div class='col-sm'><a href='$editLink'>Editar</a></div>";
            $actions .= $editAnchor;
            $deleteLink = base_url(). "api/clientes/remover/{$row->id}";
            $deleteAnchor = "<div class='col-sm'><a href=\"javascript:remover('$deleteLink');\">Remover</a></div>";
            $actions .= $deleteAnchor . '</div>';

            $tableData[] = array(
                $row->id,
                $row->nome,
                $row->cpf,
                $row->sexo == self::SEXO_MASCULINO ? 'Masculino' : 'Feminino',
                $row->email,
                $actions
            );
        }

        return $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode(array(
                'draw' => $draw,
                'recordsTotal' => $data->num_rows(),
                'recordsFiltered' => $data->num_rows(),
                'data' => $tableData,
            )));
    }

    public function create() {
        $this->load->view('templates/header');
        $this->load->view('clientes/create');
        $this->load->view('templates/footer');
    }

    public function createAPI() {
        $data = $this->input->post();
        $this->clientes_model->save($data);
        return $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode(array(
                'data' => 'OK'
            )));
    }

    public function edit()
    {
        $id = $this->uri->segment(3);
        if (empty($id)) {
            show_404();
        }
        $data = $this->clientes_model->getById($id);

        $this->load->view('templates/header');
        $this->load->view('clientes/edit', $data);
        $this->load->view('templates/footer');
    }

    public function editAPI() {
        $data = $this->input->post();
        $id = $data['id'];
        unset($data['id']);

        $this->clientes_model->update($id, $data);
        return $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode(array(
                'data' => 'OK'
            )));
    }

    public function deleteAPI() {
        $id = $this->uri->segment(4);
        $this->clientes_model->deleteById($id);

        return $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode(array('data' => 'OK')));
    }
}
