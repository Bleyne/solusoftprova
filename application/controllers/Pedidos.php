<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pedidos extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('clientes_model');
        $this->load->model('produtos_model');
        $this->load->model('pedidos_model');
        $this->load->helper('url_helper');
        $this->load->helper('util/convert_ptbr');
    }

	public function index()
	{
        $this->load->view('templates/header');
        $this->load->view('pedidos/index');
        $this->load->view('templates/footer');
	}

    public function data()
    {
        $draw = intval($this->input->get("draw"));
        $data = $this->pedidos_model->get();

        $tableData = array();
        foreach ($data->result() as $row) {
            $actions = '<div class="row">';
            $editLink = base_url(). "pedidos/editar/{$row->id}";
            $editAnchor = "<div class='col-sm'><a href='$editLink'>Editar</a></div>";
            $actions .= $editAnchor;
            $deleteLink = base_url(). "api/pedidos/remover/{$row->id}";
            $deleteAnchor = "<div class='col-sm'><a href=\"javascript:remover('$deleteLink');\">Remover</a></div>";
            $actions .= $deleteAnchor;
            $enviarEmailLink = base_url(). "email/envio/pedido/{$row->id}";
            $enviarEmailAnchor = "<div class='col-sm'><a href=\"javascript:enviarEmail('$enviarEmailLink');\">Enviar Email</a></div>";
            $actions .= $enviarEmailAnchor . '</div>';

            $tableData[] = array(
                $row->id,
                $row->nome,
                convertDatePtBr($row->data),
                $actions
            );
        }

        return $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode(array(
                'draw' => $draw,
                'recordsTotal' => $data->num_rows(),
                'recordsFiltered' => $data->num_rows(),
                'data' => $tableData,
            )));
    }

    public function create() {
        $clientes = $this->clientes_model->getSelectIdNome();
        $produtos = $this->produtos_model->getSelectIdNome();
        $data['clientes'] = $clientes;
        $data['produtos'] = $produtos;
        $this->load->view('templates/header');
        $this->load->view('pedidos/create', $data);
        $this->load->view('templates/footer');
    }

    public function createAPI() {
        $data = $this->input->post();
        $pedidoId = $this->pedidos_model->save(array(
            'cliente_id'      => $data['cliente_id'],
            'forma_pagamento' => $data['forma_pagamento'],
            'data'            => date('Y-m-d'),
            'observacao'      => $data['observacao']
        ));

        foreach ($data['produto'] as $keyIndex => $produtoPedido) {
            $this->pedidos_model->saveProdutoPedido(array(
                'pedido_id'      => $pedidoId,
                'produto_id'     => (int)$data['produto'][$keyIndex],
                'qtde'           => (int)$data['quantidade'][$keyIndex]
            ));
        }

        return $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode(array(
                'data' => 'OK'
            )));
    }

    public function edit()
    {
        $id = $this->uri->segment(3);
        if (empty($id)) {
            show_404();
        }

        $data = array();
        $result = $this->pedidos_model->getById($id)->result();

        foreach ($result as $key=>$row) {
            $data['pedido_produto']['produto_id'][] = $result[$key]->produto_id;
            $data['pedido_produto']['qtde'][]     = $result[$key]->qtde;
        }
        $data['id'] = $result[0]->id;
        $data['cliente_id'] = $result[0]->cliente_id;
        $data['observacao'] = $result[0]->observacao;
        $data['forma_pagamento'] = $result[0]->forma_pagamento;

        $clientes = $this->clientes_model->getSelectIdNome();
        $produtos = $this->produtos_model->getSelectIdNome();
        $data['clientes'] = $clientes;
        $data['produtos'] = $produtos;

        $this->load->view('templates/header');
        $this->load->view('pedidos/edit', $data);
        $this->load->view('templates/footer');
    }

    public function editAPI() {
        $data = $this->input->post();

        $id = $data['id'];

        $this->pedidos_model->update($id, array(
            'cliente_id'        => $data['cliente_id'],
            'forma_pagamento'   => $data['forma_pagamento'],
            'observacao'        => $data['observacao'],
        ));

        $this->pedidos_model->deletePedidoProdutoById($id);

        foreach ($data['produto'] as $keyIndex => $produtoPedido) {
            $this->pedidos_model->saveProdutoPedido(array(
                'pedido_id'      => $id,
                'produto_id'     => (int)$data['produto'][$keyIndex],
                'qtde'           => (int)$data['quantidade'][$keyIndex]
            ));
        }
        return $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode(array(
                'data' => 'OK'
            )));
    }

    public function deleteAPI() {
        $id = $this->uri->segment(4);
        $this->pedidos_model->deleteById($id);

        return $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode(array('data' => 'OK')));
    }

    public function sendPedidoEmail() {
        $numeroPedido = $this->uri->segment(4);
        $result = $this->pedidos_model->getClientFullOrder($numeroPedido);
        $dadosPedido = array();

        foreach ($result as $key => $row) {
            $dataPedido = $row->pedido_data;
            $clienteNome = $row->cliente_nome;
            $clienteEmail = $row->cliente_email;
            $dadosPedido[$key]['nome_produto'] = $row->nome;
            $dadosPedido[$key]['qtde'] = $row->qtde;
            $dadosPedido[$key]['valor'] = $row->valor;
        }

        $dataOutput['pedidoDetalhes'] = $dadosPedido;
        $dataOutput['pedidoData'] = $dataPedido;
        $dataOutput['numeroPedido'] = $numeroPedido;

        $html = $this->load->view('templates/pedidoEmail' , $dataOutput, true);

        $this->load->library('email');
        $config = $this->config->item('email');
        $this->email->initialize($config);

        $this->email->from($config['smtp_user'], 'Pedido Contato'); // Remetente
        $this->email->to($clienteEmail, $clienteNome); // Destinatário

        $this->email->subject("Pedido Nº $numeroPedido");
        $this->email->message($html);

        if($this->email->send())
        {
            return $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode(array('data' => 'OK')));
        }
        else
        {
            $error = $this->email->print_debugger();
            return $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode(array('error' => $error)));
        }
    }
}
