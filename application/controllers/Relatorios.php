<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Mpdf\Mpdf;

class Relatorios extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('clientes_model');
        $this->load->model('produtos_model');
        $this->load->model('pedidos_model');
        $this->load->helper('url_helper');
        $this->load->helper('util/convert_ptbr');
    }

	public function pedidos()
	{
        $this->load->view('templates/header');
        $this->load->view('relatorios/pedidos/index');
        $this->load->view('templates/footer');
	}

    public function pedidosView()
    {
        $data = $this->input->post();
        $dataInicial = convertDateMySql($data['data_inicial']);
        $dataFinal = convertDateMySql($data['data_final']);
        $dataOutput['result'] = $this->pedidos_model->getBetweenDates($dataInicial, $dataFinal)->result();
        $dataOutput['dataInicial'] = $data['data_inicial'];
        $dataOutput['dataFinal'] = $data['data_final'];
        $html = $this->load->view('relatorios/pedidos/relatorio' , $dataOutput, true);

        $mpdf = new Mpdf(array('mode' => 'utf-8', 'format' => 'A4'));
        $mpdf->SetTitle("Solusoft Relatório Pedidos");
        $mpdf->SetDisplayMode('fullpage');
        $mpdf->WriteHTML($html);
        $mpdf->Output("relatorio_pedidos.pdf", 'I');
    }

    public function clientes()
    {
        $clientes = $this->clientes_model->getSelectIdNome();
        $data['clientes'] = $clientes;

        $this->load->view('templates/header');
        $this->load->view('relatorios/clientes/index', $data);
        $this->load->view('templates/footer');
    }

    public function clientesView()
    {
        $data = $this->input->post();
        $clienteId = $data['cliente_id'];
        $result = $this->pedidos_model->getClientReport($clienteId);
        $dataOutput = array();

        //Organizar informações
        $index = 0;
        foreach ($result as $row) {
            $numeroPedido = $row->pedido_id;
            $dataOutput[$numeroPedido]['numero_pedido'] = $numeroPedido;
            $clienteNome = $row->cliente_nome;
            $dataOutput[$numeroPedido]['pedido_data'] = $row->pedido_data;
            $dataOutput[$numeroPedido]['pedido_detalhes'][$index]['produto'] = $row->nome;
            $dataOutput[$numeroPedido]['pedido_detalhes'][$index]['qtde'] = $row->qtde;
            $dataOutput[$numeroPedido]['pedido_detalhes'][$index]['valor_individual'] = $row->valor;
            $index++;
        }

        //Realizar Somatorio
        foreach ($dataOutput as $key => $output) {
            $total_pedido = 0;
            foreach ($output['pedido_detalhes'] as $detalhes) {
                $total_pedido += $detalhes['qtde'] * $detalhes['valor_individual'];
            }
            $dataOutput[$key]['total_pedido'] = convertDateMySqlconvertDinheiro($total_pedido);
        }
        $dataReport['data'] = $dataOutput;
        $dataReport['clienteNome'] = $clienteNome;
        $html = $this->load->view('relatorios/clientes/relatorio' , $dataReport, true);

        $mpdf = new Mpdf(array('mode' => 'utf-8', 'format' => 'A4'));
        $mpdf->SetTitle("Solusoft Relatório Pedidos");
        $mpdf->SetDisplayMode('fullpage');
        $mpdf->WriteHTML($html);
        $mpdf->Output("relatorio_pedidos.pdf", 'I');
    }
}
