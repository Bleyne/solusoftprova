<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Produtos extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('produtos_model');
        $this->load->helper('url_helper');
        $this->load->helper('util/convert_ptbr');
    }

	public function index()
	{
        $this->load->view('templates/header');
        $this->load->view('produtos/index');
        $this->load->view('templates/footer');
	}

    public function data()
    {
        $draw = intval($this->input->get("draw"));
        $data = $this->produtos_model->get();

        $tableData = array();
        foreach ($data->result() as $row) {
            $actions = '<div class="row">';
            $editLink = base_url(). "produtos/editar/{$row->id}";
            $editAnchor = "<div class='col-sm'><a href='$editLink'>Editar</a></div>";
            $actions .= $editAnchor;
            $deleteLink = base_url(). "api/produtos/remover/{$row->id}";
            $deleteAnchor = "<div class='col-sm'><a href=\"javascript:remover('$deleteLink');\">Remover</a></div>";
            $actions .= $deleteAnchor . '</div>';

            $tableData[] = array(
                $row->id,
                $row->nome,
                $row->cor,
                $row->tamanho,
                convertDateMySqlconvertDinheiro($row->valor),
                $actions
            );
        }

        return $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode(array(
                'draw' => $draw,
                'recordsTotal' => $data->num_rows(),
                'recordsFiltered' => $data->num_rows(),
                'data' => $tableData,
            )));
    }

    public function create() {
        $this->load->view('templates/header');
        $this->load->view('produtos/create');
        $this->load->view('templates/footer');
    }

    public function createAPI() {
        $data = $this->input->post();

        $saveData = array(
            'nome' => $data['nome'],
            'cor' => $data['cor'],
            'tamanho' => $data['tamanho'],
            'valor' => convertNumeroDecimal($data['valor']),
        );

        $this->produtos_model->save($saveData);

        return $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode(array(
                'data' => 'OK'
            )));
    }

    public function edit()
    {
        $id = $this->uri->segment(3);
        if (empty($id)) {
            show_404();
        }
        $data = $this->produtos_model->getById($id);

        $data['valor'] = convertDateMySqlconvertDinheiro($data['valor']);
        $this->load->view('templates/header');
        $this->load->view('produtos/edit', $data);
        $this->load->view('templates/footer');
    }

    public function editAPI() {
        $data = $this->input->post();
        $id = $data['id'];
        unset($data['id']);
        $data['valor'] = convertNumeroDecimal($data['valor']);

        $this->produtos_model->update($id, $data);
        return $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode(array(
                'data' => 'OK'
            )));
    }

    public function deleteAPI() {
        $id = $this->uri->segment(4);
        $this->produtos_model->deleteById($id);

        return $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode(array('data' => 'OK')));
    }
}
