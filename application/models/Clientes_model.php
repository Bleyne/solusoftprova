<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Clientes_model extends CI_Model
{

    var $table = 'clientes';

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function get()
    {
        return $this->db->get($this->table);
    }

    public function getSelectIdNome()
    {
        return $this->db->select('id,nome')
        ->get($this->table)
        ->result_array();
    }

    public function getById($id)
    {
        $query = $this->db->get_where($this->table, array('id' => $id));
        return $query->row_array();
    }

    public function save($data)
    {
        return $this->db->insert($this->table, $data);
    }

    public function update($id, $data)
    {
        $this->db->where('id', $id);
        $this->db->update($this->table, $data);
        return $this->db->affected_rows();
    }

    public function deleteById($id)
    {
        $this->db->where('id', $id);
        $this->db->delete($this->table);
    }
}
