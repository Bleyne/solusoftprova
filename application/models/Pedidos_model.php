<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pedidos_model extends CI_Model
{

    var $table = 'pedidos';
    var $tableProdutoPedidos = 'produto_pedidos';

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function get()
    {
        $this->db->select('pedidos.id, clientes.nome, pedidos.data');
        $this->db->from('pedidos');
        $this->db->join('clientes', 'clientes.id = pedidos.cliente_id');
        return $this->db->get();
    }

    public function getById($id)
    {
        $this->db->select('*');
        $this->db->from('pedidos');
        $this->db->join('produto_pedidos', 'produto_pedidos.pedido_id = pedidos.id', 'left');
        $this->db->where('pedidos.id', $id);
        return $this->db->get();
    }

    public function getClientReport($id)
    {
        $query = $this->db->query("
                SELECT 
                  p.`id` AS pedido_id, p.`data` AS pedido_data, c.`nome`, p.`cliente_id`, pr.`nome`, pp.`produto_id`, pp.`qtde`, pr.`valor`, c.`nome` AS cliente_nome
                FROM `pedidos` p 
                  LEFT JOIN `produto_pedidos` pp ON pp.`pedido_id` = p.`id` 
                  INNER JOIN `produtos` pr ON pr.`id` = pp.`produto_id` 
                  INNER JOIN `clientes` c ON c.`id` = p.`cliente_id` 
                WHERE p.`cliente_id` = $id
                ORDER BY `pedido_id`
        ");
        return $query->result();
    }

    public function getClientFullOrder($pedidoId)
    {
        $query = $this->db->query("
                SELECT 
                  p.`id` AS pedido_id, p.`data` AS pedido_data, c.`nome`, p.`cliente_id`, pr.`nome`, pp.`produto_id`, pp.`qtde`, pr.`valor`, c.`nome` AS cliente_nome, c.`email` AS cliente_email
                FROM `pedidos` p 
                  LEFT JOIN `produto_pedidos` pp ON pp.`pedido_id` = p.`id` 
                  INNER JOIN `produtos` pr ON pr.`id` = pp.`produto_id` 
                  INNER JOIN `clientes` c ON c.`id` = p.`cliente_id` 
                WHERE p.`id` = $pedidoId
        ");
        return $query->result();
    }

    public function getBetweenDates($dataInicial, $dataFinal)
    {
        $this->db->select('*');
        $this->db->from('pedidos');
        $this->db->join('produto_pedidos', 'produto_pedidos.pedido_id = pedidos.id', 'left');
        $this->db->where('pedidos.data >=', $dataInicial);
        $this->db->where('pedidos.data <=', $dataFinal);
        return $this->db->get();
    }

    public function save($data)
    {
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    public function saveProdutoPedido($data)
    {
        return $this->db->insert($this->tableProdutoPedidos, $data);
    }

    public function update($id, $data)
    {
        $this->db->where('id', $id);
        $this->db->update($this->table, $data);
        return $this->db->affected_rows();
    }

    public function deleteById($id)
    {
        $this->db->where('id', $id);
        $this->db->delete($this->table);
    }

    public function deletePedidoProdutoById($id)
    {
        $this->db->where('pedido_id', $id);
        $this->db->delete($this->tableProdutoPedidos);
    }
}
