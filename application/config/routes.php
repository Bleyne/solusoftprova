<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'clientes';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

/* Clientes Routes*/
$route['clientes'] = 'clientes';
$route['clientes/data'] = 'clientes/data';
$route['clientes/criar'] = 'clientes/create';
$route['api/clientes/criar'] = 'clientes/createAPI';
$route['clientes/editar/(:any)'] = 'clientes/edit/$1';
$route['api/clientes/editar'] = 'clientes/editAPI';
$route['api/clientes/remover/(:any)'] = 'clientes/deleteAPI';

/* Produtos Routes*/
$route['produtos'] = 'produtos';
$route['produtos/data'] = 'produtos/data';
$route['produtos/criar'] = 'produtos/create';
$route['api/produtos/criar'] = 'produtos/createAPI';
$route['produtos/editar/(:any)'] = 'produtos/edit/$1';
$route['api/produtos/editar'] = 'produtos/editAPI';
$route['api/produtos/remover/(:any)'] = 'produtos/deleteAPI';

/* Pedidos Routes*/
$route['pedidos'] = 'pedidos';
$route['pedidos/data'] = 'pedidos/data';
$route['pedidos/criar'] = 'pedidos/create';
$route['api/pedidos/criar'] = 'pedidos/createAPI';
$route['pedidos/editar/(:any)'] = 'pedidos/edit/$1';
$route['api/pedidos/editar'] = 'pedidos/editAPI';
$route['api/pedidos/remover/(:any)'] = 'pedidos/deleteAPI';

/* Relatorios Routes*/
$route['relatorio/pedidos'] = 'relatorios/pedidos';
$route['relatorio/pedidos/view'] = 'relatorios/pedidosView';
$route['relatorio/clientes'] = 'relatorios/clientes';
$route['relatorio/clientes/view'] = 'relatorios/clientesView';

/* Envio de Email*/
$route['email/envio/pedido/(:any)'] = 'pedidos/sendPedidoEmail';