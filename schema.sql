CREATE DATABASE `prova`;

USE `prova`;

/*Table structure for table `clientes` */

CREATE TABLE `clientes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(150) NOT NULL,
  `cpf` varchar(14) NOT NULL,
  `sexo` tinyint(1) NOT NULL,
  `email` varchar(150) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `pedidos` */

CREATE TABLE `pedidos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cliente_id` int(10) unsigned NOT NULL,
  `data` date NOT NULL,
  `observacao` text DEFAULT NULL,
  `forma_pagamento` enum('dinheiro','cartao','cheque') NOT NULL,
  PRIMARY KEY (`id`),
  KEY `PRODUTO_PEDIDOS_FK_3` (`cliente_id`),
  CONSTRAINT `PRODUTO_PEDIDOS_FK_3` FOREIGN KEY (`cliente_id`) REFERENCES `clientes` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `produtos` */

CREATE TABLE `produtos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(150) NOT NULL,
  `cor` varchar(40) NOT NULL,
  `tamanho` varchar(40) NOT NULL,
  `valor` decimal(10,2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `produto_pedidos` */

CREATE TABLE `produto_pedidos` (
  `pedido_id` int(10) unsigned NOT NULL,
  `produto_id` int(10) unsigned NOT NULL,
  `qtde` int(10) unsigned NOT NULL,
  PRIMARY KEY (`pedido_id`,`produto_id`),
  KEY `PRODUTO_PEDIDOS_FK_2` (`produto_id`),
  CONSTRAINT `PRODUTO_PEDIDOS_FK_1` FOREIGN KEY (`pedido_id`) REFERENCES `pedidos` (`id`) ON DELETE CASCADE,
  CONSTRAINT `PRODUTO_PEDIDOS_FK_2` FOREIGN KEY (`produto_id`) REFERENCES `produtos` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

